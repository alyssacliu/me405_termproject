"""
@file SetupDesign.py

@section sec_titleSnD Term Project Setup and Design 

@author Jacob Burghgraef and Alyssa Liu

@date June 11th, 2021

@section sec_RTP Resistive Touch Panel

We used a resistive touch panel that would allow us to write a program that 
returns the position of any object in contact with the touch panel. This is 
device we used for position and velocity data input to our controller. Acting 
like a potentiometer, or voltage divider, we scanned for the X and Y positions 
by reading the voltages throughout the panel. A schematic representation of 
the 4 wire model we used to measure voltages is shown below.

@image html "RTP_Schematic.png"

We set Xp to 3.3V and Xm to 0V, which allowed the voltage at the junction to 
be measured by one of the Y components, with the other pin acting as a 
floating pin. Through this design, we would be able to tell where the point of 
contact exists as the resistance of Xp would change and the measured voltage 
would increase or decrease proportionally. Similarly, as the voltage is 
consistent and uniform throughout the panel, then we can say the voltage 
scales linearly with position resulting in translation between voltage and 
position. A pinout table mapping the various touch panel pins to pins on the 
Nucleo is shown below.

@image html "RTP_Pinout.png"

To connect the resistive touch panel to the system, we utilized the FPC 
Breakout component that we soldered and connected to the touch panel. We then 
connected the sensor to the Nucleo via the breakout board headers via 4 pin 
extension cables. A photo of the hardware setup is shown below.

@image html "RTP_hardwareSetup.png"

@section sec_motors DC Motors

Shown below are the pin mappings from the DRV8847 motor pins to the Nucleo 
pins and timer channels.

@image html "DC_MotorConnections.png"

The 4-Pin configuration truth table below shows the state of each pin for 
various functions of the DC Motors. Note the DC motors are driven using PWM 
(Pulse Width Modulation), and that only some of these functions are 
implemented in the Motor Driver.

@image html "DC_4Pin_Config.png"

@section sec_encs Encoders

The table below shows the pinout configuration for each encoder.

@image html "encoder_pinout.png"

@section sec_file File List
@ref Touch Panel Driver: TouchPanelDriver.py
@ref Motor Driver: DRV8847.py
@ref Encoder Driver: EncoderDriver.py
"""