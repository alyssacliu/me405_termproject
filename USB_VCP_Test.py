'''
@file USB_VCP_Test.py

A file to test the functionality of USB VCP

'''

from pyb import USB_VCP

key = USB_VCP()
key.init(flow = USB_VCP.RTS)

while True:
    if key.any():
        print('I smell a key press!')
        c = key.readline().decode('utf-8')
        print('Let me guess... you pressed ', str(c))