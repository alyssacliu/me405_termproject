'''
@file ControllerTask.py

@author Alyssa Liu

@date May 31st, 2021

@brief This file is the task script for the controller.

@details The following script outlines the states and tasks for the controller.
'''

import pyb 
import TPShares, utime


class ControllerTask():
	
	init_state0 = 0

	setUp_state1 = 1

	controllerCalc_state2 = 2

	def __init__(self, interval):		

		self.interval = interval

		self.state = 0

		self.x = 0

		self.y = 0

		self.K1 = -5.7941

		self.K2 = -1.5335

		self.K3 = -1.5612

		self.K4 = -0.1296

		self.R = 2.21

		self.Kt = 13.8/1000

		self.Vdc = 12

		self.start_time = utime.ticks_us()

		self.next_interval = self.start_time + self.interval

	def run(self):
		if ((utime.ticks_diff(self.start_time, self.next_interval)) >= 0):
			if (self.state == self.init_state0):
				pyb.delay(1000)

			elif (self.state == self.setUp_state1):
				self.x = TPShares.x.get()
				self.y = TPShares.y.get()
				
				self.xdot = TPShares.xdot.get()
				self.ydot = TPShares.ydot.get()
				
				self.th_x = TPShares.th_x.get()
				self.th_y = TPShares.th_y.get()
				
				self.th_xdot = TPShares.th_xdot.get()
				self.th_ydot = TPShares.th_ydot.get()
				
				self.ty = 0
				self.tx = 0

				self.state = self.controllerCalc_state2
			
			elif (self.state == self.controllerCalc_state2):
				self.tx = (self.K1 * self.x) + (self.K2 * self.th_y) + (self.K3 * self.xdot) + (self.K4 * self.th_ydot)
				self.ty = (self.K1 * self.y) + (self.K2 * self.th_x) + (self.K3 * self.ydot) + (self.K4 * self.th_xdot)

				TPShares.dtyX.put(((100 * self.R) / (4 * self.Kt * self.Vdc)) * self.tx)
				TPShares.dtyY.put(((100 * self.R) / (4 * self.Kt * self.Vdc)) * self.ty)

			else:
				pass

			self.next_interval = self.start_time + self.interval

			yield(self.state)

