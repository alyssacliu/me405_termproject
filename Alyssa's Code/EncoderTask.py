'''
@file EncoderTask.py

@author Alyssa Liu

@date May 31st, 2021

@brief This file is the task script for the encoder.

@details The following script outlines the states and tasks for the encoder.
'''

import pyb 
import TPShares, utime


class EncoderTask():
	
	platform_length = 100e-3

	motorArm_length = 60e-3

	init_state0 = 0

	calibrate_state1 = 1

	encoder_state2 = 2

	def __init__(self, encoder1, encoder2, RTP, IMU, interval):		
		self.encoder1 = encoder1

		self.encoder2 = encoder2

		self.interval = interval

		self.state = 0

		self.RTP = RTP

		self.IMU = IMU

		self.start_time = utime.ticks_us()

		self.next_interval = self.start_time + self.interval

	def run(self):
		if ((utime.ticks_diff(self.start_time, self.next_interval)) >= 0):
			if (self.state == self.init_state0):
				pyb.delay(1000)

			elif (self.state == self.calibrate_state1):
				euler = self.IMU.euler()

				self.encoder1.update()
				self.encoder2.update()
				
				if abs(euler[1]) <= 0.0625 and  self.encoder1_calibrated == False:
					self.encoder1.set_position(0)
					self.encoder1_calibrated = True

				if abs(euler[2]) <= 0.0625 and self.encoder2_calibrated == False:
					self.encoder2.set_position(0)
					self.encoder2_calibrated = True

				if self.encoder1_calibrated and self.encoder2_calibrated:
					self.state = self.encoder_state2
			
			elif (self.state == self.encoder_state2):

				TPShares.th_x.put((-1)*(self.encoder1.get_position() * 2 * 3.14159 / 4000) * self.motorArm_length / self.platform_length)
				TPShares.th_y.put((-1)*(self.encoder2.get_position() * 2 * 3.14159 / 4000) * self.motorArm_length / self.platform_length)
				        
				TPShares.th_xdot.put((-1)*(self.encoder2.get_delta() * 1000 * 2 * 3.14159 / (self.interval * 4000)) * self.motorArm_length / self.platform_length)
				TPShares.th_ydot.put((-1)*(self.encoder1.get_delta() * 1000 * 2 * 3.14159 / (self.interval * 4000)) * self.motorArm_length / self.platform_length)

			else:
				pass

			self.next_interval = self.start_time + self.interval

			yield(self.state)

