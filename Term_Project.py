"""
@file Term_Project.py

@page ME405TermProject

@section sec_titleTPLP ME 405 Term Project Landing Page

@author Jacob Burghgraef and Alyssa Liu

@date June 11th, 2021

@section sec_intro Introduction

This term project is the culmination of all the work I did in my core 
Mechatronics classes (ME 305 and ME 405). While this project was technically 
assigned in ME 405, much of the groundwork and prerequisite knowledge was 
built up in ME 305. Thus, it's better to think of this project as being a 
culmination of both classes rather than project for just one class. Moreover, 
I did a similar, less complicated term project (lab 7) in ME 305, so this can 
be considered as an extension of that project.

@section sec_summ Summary

Our final term project consisted of creating a mechanical closed loop system 
that would properly adjust positions via two DC motors to balance a weighted 
ball on a resistive touch panel. To properly pace ourselves through this 
project, we were recommended to complete 4 components over the remainder of 
the quarter, which provided us with a week duration to complete each element 
of the system. 

The components were:
    1. Reading from Resistive Touch Panels
    2. Driving DC Motors with PWM and H-bridges
    3. Reading from Quadrature Encoders
    4. Balancing the Ball

Through using hardware shipped to our location, we soldered and assembled the 
system and began to intermittently develop our code.

@section sec_SC System Calculations

The calculations for this system are covered here: EOMDerivation.py \ref

@section sec_setup Project Setup and Design

The setup is covered here: SetupDesign.py \ref

@section sec_EngReq Engineering Requirements

The engineering requirements for this system are covered here: EngReq.py \ref

@section sec_DCMotors DC Motors: Fault Detection

How fault detection was handled is covered here: DCMotorsFD.py \ref

@section sec_Calib Calibration and Tuning

The calibration for the touch panel is covered here: TPCalibration.py \ref

@section sec_taskOv Overview of Tasks

The general overview of each task is covered here: TasksOverview.py \ref

@section sec_obs Obstacles 

Throughout this lab, we found that we had some issues with the hardware. For a 
period of time, we found that between the two of us, we had a malfunctioning 
touch panel and an incorrectly/missing component from our respective lab kits. 
However, one of us was able to acquire a new touch panel that ended up working, 
so only one hardware kit was suitable for testing. Similarly, we also found 
that neither of our motors would properly adjust the plate due to the lack of 
friction and inability to properly tighten the motor arm components around the 
axles without stripping the screws. As a result, the motor arms would not 
interact with the motors properly and the plate would not make the correct and 
smooth adjustments. To fix this, we tried retightening the screws around the 
motor arm to little success. Additionally, we tried placing a paper ‘shim’ in 
between the motor arm clamp and the motor shaft to get contact, but that also 
proved unsuccessful. As a last resort, we used superglue to fix the motor arm 
to the output shaft, which worked well enough to allow us to continue testing. 
The greatest obstacle we faced was time - since we were able to fix the motor 
arm to the output shaft the day before the final project deadline, there 
wasn’t enough time to properly tune the controller and fully debug the code. 
However, we managed to get the balancing platform in a semi-functional state, 
which is explained in further detail in the results section.

@section sec_TPresults Results and Discussion

The Results, Video, and Discussion can be found here: Results.py \ref

@section sec_src Source Code

The code for the entirety of the project can be located here: 
    https://bitbucket.org/alyssacliu/me405_termproject/src/master/
"""