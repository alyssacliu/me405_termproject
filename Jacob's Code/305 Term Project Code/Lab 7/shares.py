'''
@file shares.py
@package Lab_7
@brief A container for all the inter-task variables
@author Jacob Burghgraef, Modified from Charlie Refvem's shares.py file
@date December 4, 2020
The documentation for this code can be found at: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%207/shares.py
'''

## The kp value sent from the user interface task to the controller task
kp          = None

## The response from the controller task after measuring velocity data
w_meas      = None

## The response from the controller task after measuring position data
theta       = None

## The reference omega values from the reference csv file
w_ref       = None