''' 
@file Motor_Driver.py

@brief A driver for a Motor

@details    This motor driver initializes 3 pins and a timer for PWM 
            generation. The nSLEEP pin allows the motor to be turned enabled
            and disabled on command, and the 2 other pins, IN1 and IN2, are
            the input to the half bridges controlling the motor. This driver 
            only has foreward and reverse functionality, and will throw an 
            error message when the inputted duty cycle exceeds 100% in either
            direction.

@author Jacob Burghgraef

@date November 10, 2020

The source code for this file can be found here: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%206/Motor_Driver.py
'''

import pyb

class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''

    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
            pins and turning the motor off for safety.
            @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
            @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
            @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
            @param timer A pyb.Timer object to use for PWM generation on
            IN1_pin and IN2_pin. '''
        #print ('Creating a motor driver')
        
        self.nSLEEP_pin = nSLEEP_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        
        self.nSLEEP_pin.low()

    def enable (self):
        '''
        @brief Turns motor on using nSleep pin
        '''
        self.nSLEEP_pin.high()
        #print ('Enabling Motor')

    def disable (self):
        '''
        @brief Turns motor off using nSleep pin
        '''
        self.nSLEEP_pin.low()
        #print ('Disabling Motor')

    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
            to the motor to the given level. Positive values
            cause effort in one direction, negative values
            in the opposite direction.
            @param duty A signed integer holding the duty
                cycle of the PWM signal sent to the motor '''
        try:
            self.duty = int(duty)
            #print('Setting Duty Cycle to ' + str(self.duty))
        except ValueError:
            print('Invalid input, the duty cycle must be an integer between -100 and +100')
            
        self.timch1 = self.timer.channel(1, pyb.Timer.PWM, pin = self.IN1_pin)
        self.timch2 = self.timer.channel(2, pyb.Timer.PWM, pin = self.IN2_pin)
        
        if self.duty >= 0 and self.duty <= 100:
            # Spin the Motor in one direction or not at all
            self.timch1.pulse_width_percent(self.duty)
            self.timch2.pulse_width_percent(0)
        
        elif self.duty < 0 and self.duty >= -100:
            # Spin the Motor in the opposite direction
            self.timch1.pulse_width_percent(0)
            self.timch2.pulse_width_percent(abs(self.duty))
        
        else:
            # Error State
            print('Invalid input, the duty cycle must be an integer between -100 and +100')
            
if __name__ == '__main__':
# Adjust the following code to write a test program for your motor class. Any
# code within the if __name__ == '__main__' block will only run when the
# script is executed as a standalone program. If the script is imported as
# a module the code block will not run.

    # Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP);
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4);
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5);

    # Create the timer object used for PWM generation
    tim = pyb.Timer(3, freq = 20000);

    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)

    # Enable the motor driver
    moe.enable()

    # Set the duty cycle to 10 percent
    moe.set_duty(35)
    