'''
@file HW0x02.py

@author Jacob Burghgraef

@section sec_HW0x02 Balancing Plate Hand Calculations

The following hand calculations derive equations of motion for a rotating 
platform that balances a ball.

@image html "HW0x02 pg 1.JPG" width=50%
@image html "HW0x02 pg 2.JPG" width=50%
@image html "HW0x02 pg 3.JPG" width=50%
@image html "HW0x02 pg 4.JPG" width=50%
@image html "HW0x02 pg 5.JPG" width=50%
@image html "HW0x02 pg 6.JPG" width=50%
'''